import React from "react";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux'
import { createStore } from "redux"
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter, Route } from 'react-router-dom'
import reducer from "./reducer"
import { composeWithDevTools } from 'redux-devtools-extension'

const store = createStore(
    reducer, 
    composeWithDevTools()
    )

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <Route component={App}></Route>
        </BrowserRouter>
    </Provider>,
    document.getElementById("root")
);

serviceWorker.unregister();