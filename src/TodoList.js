import React, { Component } from "react";
import TodoItem from "./TodoItem.js"
import { connect } from "react-redux"
import { deleteTodo, toggleTodo } from "./action"
class TodoList extends Component {
    render() {
        return (
            <section className="main">
                <ul className="todo-list">
                    {this.props.todos.map(todo => (
                        <TodoItem 
                            key={todo.id}
                            title={todo.title}
                            completed={todo.completed}
                            handleToggleClick={(event) => this.props.toggleTodo(todo)}
                            handleDestroyClick={(event) => this.props.deleteTodo(todo.id)} />
                    ))}
                </ul>
            </section>
        );
    }
}
const mapDispatchToProps = {
    deleteTodo,
    toggleTodo
};
export default connect(null, mapDispatchToProps)(TodoList)