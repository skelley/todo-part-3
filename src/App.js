import React, { Component } from "react";
import "./index.css";
import TodoList from "./TodoList.js"
import { Route, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { clearCompletedTodos, addTodo } from "./action"


class App extends Component {
  state = {
    input: ""
  }

  handleCreateTodo = event => {
    if (event.keyCode === 13) {
      // console.log('hi')
      this.props.addTodo(event.target.value)
      event.target.value = ''
    }
  }
  handleChange = event => {
    this.setState({ input: event.target.value })
    // console.log(this.state)
  }
  render() {
    //this.props.todos
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            value={this.input}
            onChange={this.handleChange}
            onKeyDown={this.handleCreateTodo}
          />
        </header>
        <Route exact path='/' render={() => (
          <TodoList todos={this.props.todos}
            handleToggleClick={this.handleToggleClick} />
        )} />
        <Route path='/active' render={() => (
          <TodoList todos={this.props.todos.filter(todo => {
            if (todo.completed === false) {
              return todo
            }
            return false
          })}
            handleToggleClick={this.handleToggleClick} />
        )} />
        <Route path='/completed' render={() => (
          <TodoList todos={this.props.todos.filter(todo => {
            if (todo.completed === true) {
              return todo
            }
            return false
          }
          )}
            handleToggleClick={this.handleToggleClick} />
        )} />
        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
            <strong>{this.props.todos.filter(todo => {
              if (todo.completed === false) {
                return todo
              }
              return false
            }).length}</strong> item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink exact to='/' activeClassName='selected'>All</NavLink>
            </li>
            <li>
              <NavLink to="/active" activeClassName='selected'>Active</NavLink>
            </li>
            <li>
              <NavLink to="/completed" activeClassName='selected'>Completed</NavLink>
            </li>
          </ul>
          <button className="clear-completed" onClick={event => this.props.clearCompletedTodos()}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    todos: state.todos
  }
}

const mapDispatchToProps = {
  clearCompletedTodos,
  addTodo
};

// const mapDispatchToProps = {deleteTodo: deleteTodo}
export default connect(mapStateToProps, mapDispatchToProps)(App)
